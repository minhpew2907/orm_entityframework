﻿using Microsoft.EntityFrameworkCore;


CarDbContext context = new CarDbContext();
Car lambo = new Car
{
    Make = "Lamborghini",
    Model = "Huracan",
    Year = 2021,
};

context.Cars.Add(lambo);
context.SaveChanges(); //Luu vao csdl

List<Car> cars = context.Cars.ToList();

foreach (Car c in cars)
{
    Console.WriteLine($"{c.Make} {c.Model} {c.Year}");
}


public class CarDbContext : DbContext
{
    public DbSet<Car> Cars { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        //Cai dat voi SQL server
        optionsBuilder.UseSqlServer("Data Source=DESKTOP-O6PD73S\\MINH;Initial Catalog=HelloEF1;Integrated Security=True;Persist Security Info=False;Pooling=False;Multiple Active Result Sets=False;Encrypt=True;Trust Server Certificate=True;Command Timeout=0");
    }
}

public class Car
{
    public int Id { get; set; }
    public string Make { get; set; }
    public string Model { get; set; }
    public int Year { get; set; }
}